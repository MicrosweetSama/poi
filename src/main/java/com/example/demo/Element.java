package com.example.demo;

import lombok.Data;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

@Data
public class Element {
    private String paragraphText;
    private ParagraphAlignment align;
    private String headingLevel;

}
