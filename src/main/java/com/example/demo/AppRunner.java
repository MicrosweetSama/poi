package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

//    @Autowired
//    ExportWord exportWord;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception{
        new ExportWord();

    }
}
