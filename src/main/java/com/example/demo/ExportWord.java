package com.example.demo;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.io.*;
import java.math.BigInteger;
import java.util.List;

public class ExportWord {

    public ExportWord() {
        XWPFDocument doc = readWordDoc();
        setPageSize(doc);
        setHeader(doc);
        setFooter(doc);

        // 删除模板中的内容
        List<IBodyElement> par = doc.getBodyElements();
        for(int i=par.size() - 1; i>=0 ; i--){
//            System.out.println(doc.getParagraphs().get(i).getFirstLineIndent());
//            System.out.println(doc.getParagraphs().get(i).getText() + ":" + doc.getParagraphs().get(i).getStyle());
//            System.out.println(doc.getParagraphs().get(i));
            doc.removeBodyElement(i);
        }

        Element element= new Element();
        // 设置标题(1-8级)
        for(int i=2; i<10; i++){
            element.setAlign(ParagraphAlignment.CENTER);
            element.setHeadingLevel(String.valueOf(i));
            element.setParagraphText("标题" + (i-1));
            addHeading(doc, element);
        }
        // 插入正文段落
        element = new Element();
        element.setParagraphText("这是一段测试文字这是一段测试文字这是一段测试文字这是一段测试文字这是一段测试文字这是一段测试文字这是一段测试文字");
        addParagraph(doc, element);

        // 插入脚注、尾注
        testNote(doc);

        // 插入表格
        createTable(doc);

        try {
            // 插入图片
            insertPic(doc);
            // 写入docx
            saveDocument(doc, "/home/microsweet/test/ExportDemo.docx");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    // 创建一个空白文档
    public XWPFDocument createWordDoc(){
        XWPFDocument doc = new XWPFDocument();
        return doc;
    }

    // 读取模板文档
    public XWPFDocument readWordDoc(){
        XWPFDocument doc = new XWPFDocument();
        try {
            doc = new XWPFDocument(new FileInputStream("/home/microsweet/test/Module.docx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    // 幅面
    public void setPageSize(XWPFDocument doc){
        CTDocument1 document1 = doc.getDocument();
        CTBody body = document1.getBody();
        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }
        CTSectPr sectPr = body.getSectPr();
        if(!sectPr.isSetPgSz()){
            sectPr.addNewPgSz();
        }
        CTPageSz pagesize = sectPr.getPgSz();
//        pagesize.setOrient(STPageOrientation.LANDSCAPE);
        // 纸张尺寸56.7 * 297
        pagesize.setW(BigInteger.valueOf(11907));
        pagesize.setH(BigInteger.valueOf(16839));

        // 设置页边距
        CTPageMar pageMar = sectPr.getPgMar();
        pageMar.setTop(BigInteger.valueOf((int)(37*56.7)));
        pageMar.setLeft(BigInteger.valueOf((int)(28*56.7)));
        pageMar.setBottom(BigInteger.valueOf((int)(35*56.7)));
        pageMar.setRight(BigInteger.valueOf((int)(26*56.7)));
    }

    // 插入表格
    public void createTable(XWPFDocument doc){
        XWPFTable table = doc.createTable(3, 3);
    }

    public void insertPic(XWPFDocument doc) throws IOException, InvalidFormatException {
        InputStream stream = new FileInputStream("/home/microsweet/2021-11-18_09-03.png");
        XWPFParagraph paragraph = doc.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.addPicture(stream, XWPFDocument.PICTURE_TYPE_PNG, "Generated", Units.toEMU(256), Units.toEMU(256));
    }

    // 创建脚注
    static BigInteger createFootnote(XWPFDocument document, String footnoteText) {
        XWPFFootnotes footnotes = document.createFootnotes();
        CTFtnEdn ctFtnEdn = CTFtnEdn.Factory.newInstance();
        BigInteger footnoteId = BigInteger.valueOf(footnotes.getFootnotesList().size());
        ctFtnEdn.setId(footnoteId);
        XWPFFootnote footnote = footnotes.addFootnote(ctFtnEdn);
        XWPFParagraph paragraph = footnote.addNewParagraph(CTP.Factory.newInstance());
        XWPFRun run=paragraph.createRun();
        run.getCTR().addNewFootnoteRef();
        run.getCTR().addNewRPr().addNewRStyle().setVal("FootnoteReference");
        run = paragraph.createRun();
        run.setText(footnoteText);
        return footnoteId;
    }

    // 创建尾注
    static BigInteger createEndnote(XWPFDocument doc, String endnoteText){
        XWPFEndnotes endnotes = doc.createEndnotes();
        CTFtnEdn ctFtnEdn = CTFtnEdn.Factory.newInstance();
        BigInteger endnoteId = BigInteger.valueOf(endnotes.getEndnotesList().size());
        ctFtnEdn.setId(endnoteId);
        XWPFEndnote endnote = endnotes.addEndnote(ctFtnEdn);
        XWPFParagraph paragraph = endnote.addNewParagraph(CTP.Factory.newInstance());
        XWPFRun run = paragraph.createRun();
        run.getCTR().addNewRPr().addNewRStyle().setVal("EndnoteReference");
        run = paragraph.createRun();
        run.setText(endnoteText);
        return endnoteId;

    }

    public void setHeader(XWPFDocument doc){
        XWPFHeaderFooterPolicy headerFooter = doc.getHeaderFooterPolicy();
        if(headerFooter == null){
            doc.createHeaderFooterPolicy();
        }
        XWPFHeader header = headerFooter.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        XWPFParagraph paragraph = header.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun run = paragraph.createRun();
        run.setText("页眉");
    }

    public void setFooter(XWPFDocument doc){
        XWPFHeaderFooterPolicy headerFooter = doc.getHeaderFooterPolicy();
        if(headerFooter == null){
            doc.createHeaderFooterPolicy();
        }
        XWPFFooter footer = headerFooter.createFooter(XWPFHeaderFooterPolicy.DEFAULT);

        XWPFParagraph paragraph = footer.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun run = paragraph.createRun();
        run.setText("页脚");
    }

    // 添加标题
    public void addHeading(XWPFDocument doc, Element element){
        XWPFParagraph heading = doc.createParagraph();
        heading.setAlignment(element.getAlign());
        heading.setStyle(element.getHeadingLevel());

        XWPFRun run = heading.createRun();
        run.setText(element.getParagraphText());
    }

    // 添加段落
    public void addParagraph(XWPFDocument doc, Element element){
        XWPFParagraph paragraph = doc.createParagraph();
        paragraph.setStyle("af3");
        // 设置分隔线
//        paragraph.setBorderBottom(Borders.THIN_THICK_SMALL_GAP);
        // 首行缩进2字符40*字号
        paragraph.setFirstLineIndent(40*16);

        // 设置段前间距
        paragraph.setSpacingBeforeLines(100);
        // 设置段后间距
        paragraph.setSpacingAfterLines(100);

        //设置行距
        CTPPr ppr = paragraph.getCTP().getPPr();
        if(ppr == null) {
            ppr.addNewSectPr();
        }
        CTSpacing spacing = ppr.isSetSpacing()?ppr.getSpacing():ppr.addNewSpacing();
        spacing.setLineRule(STLineSpacingRule.AUTO);
        // 单倍240 多倍240*n
        spacing.setLine(BigInteger.valueOf(720));

        XWPFRun run = paragraph.createRun();
        run.setText(element.getParagraphText());
        // 设置字体
        run.setFontFamily("宋体");
        // 设置字号
        run.setFontSize(16);
        // 设置字体颜色
        run.setColor("eeeeee");
    }

    public void testNote(XWPFDocument document){
        XWPFStyles styles = document.createStyles();
        XWPFStyle style = new XWPFStyle(CTStyle.Factory.newInstance(), styles);
        style.getCTStyle().setType(STStyleType.CHARACTER);
        style.getCTStyle().setStyleId("FootnoteReference");
        style.getCTStyle().addNewRPr().addNewVertAlign().setVal(STVerticalAlignRun.SUPERSCRIPT);
        styles.addStyle(style);

        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("The text");

        paragraph = document.createParagraph();
        run = paragraph.createRun();
        run.setText("This is the text run having the first footnote");
        String footnoteText = "The content of the first footnote.";
        BigInteger footnoteId  = createFootnote(document, footnoteText);
        run = paragraph.createRun();
        run.getCTR().addNewRPr().addNewRStyle().setVal("FootnoteReference");
        run.getCTR().addNewFootnoteReference().setId(footnoteId);

        run = paragraph.createRun();
        run.setText(" further text comes here and a second footnote");
        footnoteText = "The content of the second footnote.";
        footnoteId  = createFootnote(document, footnoteText);
        run = paragraph.createRun();
        run.getCTR().addNewRPr().addNewRStyle().setVal("FootnoteReference");
        run.getCTR().addNewFootnoteReference().setId(footnoteId);

        run = paragraph.createRun();
        run.setText(" further text comes here and a third footnote");
        footnoteText = "The content of the third footnote.";
        footnoteId  = createFootnote(document, footnoteText);
        run = paragraph.createRun();
        run.getCTR().addNewRPr().addNewRStyle().setVal("FootnoteReference");
        run.getCTR().addNewFootnoteReference().setId(footnoteId);

        run = paragraph.createRun();
        run.setText(" and now the paragraph ends here.");
        BigInteger endnoteId = createEndnote(document, "test endnote");
        run = paragraph.createRun();
        run.getCTR().addNewRPr().addNewRStyle().setVal("FootnoteReference");
        run.getCTR().addNewEndnoteReference().setId(endnoteId);

        paragraph = document.createParagraph();
    }

    // 添加自定义标题style
    private static void addHeadingStyle(XWPFDocument doc, String strStyleId, int headingLevel){
        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);

        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        ctStyle.setQFormat(onoffnull);

        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        XWPFStyles styles = doc.createStyles();

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }


    // 写入word
    public void saveDocument(XWPFDocument document, String savePath) throws IOException {
        OutputStream os = new FileOutputStream(savePath);
        document.write(os);
        os.close();
    }
}
// https://stackoverflow.com/questions/39939057/adding-footnotes-to-a-word-document/56745046
